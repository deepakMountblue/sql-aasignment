create table clients (
    id INT NOT NULL PRIMARY KEY ,
    client_name VARCHAR(100),
    client_location VARCHAR(100)
);

create table manager (
    id INT NOT NULL PRIMARY KEY,
    manager_name VARCHAR(100),
    manager_location VARCHAR(100)
);

create table contracts (
    id INT NOT NULL PRIMARY KEY,
    estimation_cost INT NOT NULL,
    completion_date DATETIME NOT NULL
    FOREIGN KEY (contract_id) REFERENCES clients (client_id) 
);

create table staff (
    id INT NOT NULL PRIMARY KEY,
    staff_name VARCHAR(100),
    staff_location VARCHAR(100)
);

create table manager_client (
    id INT NOT NULL PRIMARY KEY,
    client_id INT NOT NULL,
    manager_id INT NOT NULL,
    FOREIGN KEY (client_id) REFERENCES clients (id),
    FOREIGN KEY (manager_id) REFERENCES manager (id)

)

create table manager_staff(
    id INT NOT NULL PRIMARY KEY,
    manager_id INT NOT NULL,
    staff_id INT NOT NULL,
    FOREIGN KEY (manager_id) REFERENCES managers (id),
    FOREIGN KEY (staff_id) REFERENCES staff (id)

)