create table patient(
    id INT NOT NULL PRIMARY KEY,
    patient_name VARCHAR(255)
    DOB VARCHAR(255),
    addr VARCHAR(255),
)

create table prescription (
    id INT NOT NULL PRIMARY KEY,
    drug VARCHAR(255),
    prescription_date DATETIME NOT NULL,
    dosage VARCHAR(40),
    doctor VARCHAR(70),
    secretary varchar(70)
);
 
 create table prescription_patient (
     id INT NOT NULL PRIMARY KEY,
    patient_id INT NOT NULL,
    prescription_id INT NOT NULL,
    FOREIGN KEY (patient_id) REFERENCES patients (id),
    FOREIGN KEY (prescription_id) REFERENCES prescriptions (id)

 )