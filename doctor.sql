create table doctor(
    id INT not null PRIMARY KEY,
    doctor_name varchar(255),
    Secretary varchar(255)
    
)

create table patients (
    id INT NOT NULL PRIMARY KEY,
    doctor_id INT NOT NULL,
    patient_name VARCHAR(50) NOT NULL,
    patient_dob DATETIME NOT NULL,
    patient_address VARCHAR(255),
    prescription_id INT NOT NULL,
    FOREIGN KEY (doctor_id) REFERENCES doctors (id),
    FOREIGN KEY (prescription_id) REFERENCES prescriptions (id)

);

create table prescription (
    id INT NOT NULL PRIMARY KEY,
    drug VARCHAR(255),
    prescription_date DATETIME NOT NULL,
    dosage VARCHAR(50)
);

