create table branch(
    id INT NOT NULL PRIMARY KEY,
    pincode int ,
    branch_address varchar (255),
    FOREIGN KEY (pincode) REFERENCES cities (pincode),
    FOREIGN Key (isbn) REFERENCES book (isbn)
)

create table cities(
    pincode INT not NULL PRIMARY KEY,
    city_name varchar(255),
)

create table book(
    isbn INT NOT NULL PRIMARY KEY UNIQUE,
    title VARCHAR(255),
    num_copies INT ,
    author VARCHAR(255),
    publisher varchar(255)
)

CREATE TABLE books_branches (
    books_branches_id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    book_id INT NOT NULL,
    branch_id INT NOT NULL,
    FOREIGN KEY (book_id) REFERENCES books (Isbn),
    FOREIGN KEY (branch_id) REFERENCES branch (branch_id)
);
